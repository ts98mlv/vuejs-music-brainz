import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';

import Homepage from './components/Homepage.vue';
import NotFound from "./components/NotFound.vue";
import ResultSingle from "./components/ResultSingle";
import ResultAlbum from "./components/ResultAlbum";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [{
      path: "/",
      component: Homepage
    },
    {
      path: "/404",
      component: NotFound
    },
    {
      path: "/singles/:artisteId",
      component: ResultSingle,
      name: "singles"
    },
    {
      path: "/albums/:artisteId",
      component: ResultAlbum,
      name: "albums"
    }
  ]
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
